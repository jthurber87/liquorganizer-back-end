# Liquorganizer

Liquorganizer is a clean, simple inventory system for bars, restaurants, and individuals alike


## User Stories

1. User should be able to add to inventory, using bottles.js model which includes spirit, brand, count and notes
2. User should be able to see flippable cards reflecting inventory on index page, with basic info (spirit, brand and count) on front and more detail (notes, edit, delete) on back
3. User should be able to edit information about inventory in edit page
4. User should be able to see what inventory is out of stock by color of card(?)

## Wireframe

<img src="https://imgur.com/wGlDtJi" alt="Landing Page" />



## Technologies



## Contributing

Jay Thurber
Dae Young Hwang
Isabel Luk

## Code Snippets
